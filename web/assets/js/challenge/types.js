'use strict';
var jsTypes = {

    initialize : function (){
        var base = this;
        jQuery(
            function ($) {
                base.ready();
            }
        );
    },

    ready : function (){
        var base = this;
        $(document).on('click', '.deleteTypes', function(e) { $('.confirmRmvTypes').attr('data-id', $(this).attr('data-id')); });
        $(document).on('click', '.confirmRmvTypes', function(e) { base.remTypes( $(this)); });
    },
	
    remTypes: function( _elt ){
        if( _elt.attr('data-id') >0 ){
            $.post( delTypesUrl, { 'iTypes': _elt.attr('data-id') } ).done(function( data ) {
            	$('#confRemTypes').modal('toggle');
                window.location.href = listTypesUrl;
            });
        }
    },
};
jsTypes.initialize();