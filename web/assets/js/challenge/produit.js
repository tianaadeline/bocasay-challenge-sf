'use strict';
var jsProduit = {

    initialize : function (){
        var base = this;
        jQuery(
            function ($) {
                base.ready();
            }
        );
    },

    ready : function (){
        var base = this;
        $(document).on('click', '.deleteProduit', function(e) { $('.confirmRmvProduit').attr('data-id', $(this).attr('data-id')); });
        $(document).on('click', '.confirmRmvProduit', function(e) { base.remProduit( $(this)); });
    },
	
    remProduit: function( _elt ){
        if( _elt.attr('data-id') >0 ){
            $.post( delProduitUrl, { 'iProduit': _elt.attr('data-id') } ).done(function( data ) {
            	$('#confRemProduit').modal('toggle');
                window.location.href = listProduitUrl;
            });
        }
    },
};
jsProduit.initialize();