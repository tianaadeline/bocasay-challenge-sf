'use strict';
var jsFournisseur = {

    initialize : function (){
        var base = this;
        jQuery(
            function ($) {
                base.ready();
            }
        );
    },

    ready : function (){
        var base = this;
        $(document).on('click', '.deleteFournisseur', function(e) { $('.confirmRmvFournisseur').attr('data-id', $(this).attr('data-id')); });
        $(document).on('click', '.confirmRmvFournisseur', function(e) { base.remFournisseur( $(this)); });
    },
	
    remFournisseur: function( _elt ){
        if( _elt.attr('data-id') >0 ){
            $.post( delFournisseurUrl, { 'iFournisseur': _elt.attr('data-id') } ).done(function( data ) {
            	$('#confRemFournisseur').modal('toggle');
                window.location.href = listFournisseurUrl;
            });
        }
    },
};
jsFournisseur.initialize();