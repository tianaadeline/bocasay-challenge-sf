'use strict';
var jsMarque = {

    initialize : function (){
        var base = this;
        jQuery(
            function ($) {
                base.ready();
            }
        );
    },

    ready : function (){
        var base = this;
        $(document).on('click', '.deleteMarque', function(e) { $('.confirmRmvMarque').attr('data-id', $(this).attr('data-id')); });
        $(document).on('click', '.confirmRmvMarque', function(e) { base.remMarque( $(this)); });
    },
	
    remMarque: function( _elt ){
        if( _elt.attr('data-id') >0 ){
            $.post( delMarqueUrl, { 'iMarque': _elt.attr('data-id') } ).done(function( data ) {
            	$('#confRemMarque').modal('toggle');
                window.location.href = listMarqueUrl;
            });
        }
    },
};
jsMarque.initialize();