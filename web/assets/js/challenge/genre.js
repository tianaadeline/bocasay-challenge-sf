'use strict';
var jsGenre = {

    initialize : function (){
        var base = this;
        jQuery(
            function ($) {
                base.ready();
            }
        );
    },

    ready : function (){
        var base = this;
        $(document).on('click', '.deleteGenre', function(e) { $('.confirmRmvGenre').attr('data-id', $(this).attr('data-id')); });
        $(document).on('click', '.confirmRmvGenre', function(e) { base.remGenre( $(this)); });
    },
	
    remGenre: function( _elt ){
        if( _elt.attr('data-id') >0 ){
            $.post( delGenreUrl, { 'iGenre': _elt.attr('data-id') } ).done(function( data ) {
            	$('#confRemGenre').modal('toggle');
                window.location.href = listGenreUrl;
            });
        }
    },
};
jsGenre.initialize();