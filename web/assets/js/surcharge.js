/**
 * @package:        Challenge
 * @description:    Challenge 
 * @author:         Alitiana
 * @creation:       09/08/17
 * @version:        1.0
 */
'use strict';

var InitScript = {
    /**
     * Initialisation
     */
    initialize: function() {
        var base = this;

        // Ecoute du DOM ready
        jQuery(
            function() {
                base.ready();
            }
        );
    },

    /**
     * DOM Ready
     */
    ready: function() {
        var base = this;
        base.surcharge();

        jQuery(window).resize(function() {
            
        });

    },

    surcharge: function() {
        var base = this;
        $(".cm-submenu").click(function(){
            $(".cm-menu-items li").removeAttr("style");
        });
        
       
    },



};
// initialisation de l'application
InitScript.initialize();