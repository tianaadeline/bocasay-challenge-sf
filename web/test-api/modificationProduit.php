<?php 
include 'Api.php';

$oApi = new Api();

	$iProduitId = (isset($_GET['id']))?$_GET['id']:0;
	
	
	//detail produit
	$tParamApiProduit = array("id"=>$iProduitId);
	$tRetourApiProduit = $oApi->fetchData('api-get-produit', $tParamApiProduit);	

	$oProduit = array();
	if($tRetourApiProduit->head->code == 200)
	{
		$oProduit = $tRetourApiProduit->result;
	}
	else
	{
		echo "Error: ".$tRetourApiProduit->head->message.'<br />';
	}
	//echo "<pre>";print_r($oProduit); echo "</pre>";
	

	//liste types
	$tParamApiTypes = array();
	$tRetourApiTypes = $oApi->fetchData('api-get-types', $tParamApiTypes);	

	$toTypes = array();
	if($tRetourApiTypes->head->code == 200)
	{
		$toTypes = $tRetourApiTypes->result;
	}
	else
	{
		echo "Error: ".$tRetourApiTypes->head->message.'<br />';
	}
	
	//liste genre
	$tParamApiGenre = array();
	$tRetourApiGenre = $oApi->fetchData('api-get-genres', $tParamApiGenre);		
	$toGenre = array();
	if($tRetourApiGenre->head->code == 200)
	{
		$toGenre = $tRetourApiGenre->result;
	}
	else
	{
		echo "Error: ".$tRetourApiGenre->head->message.'<br />';
	}
	
	
	//liste marque
	$tParamApiMarque = array();
	$tRetourApiMarque = $oApi->fetchData('api-get-marques', $tParamApiMarque);		
	$toMarque = array();
	if($tRetourApiMarque->head->code == 200)
	{
		$toMarque = $tRetourApiMarque->result;
	}
	else
	{
		echo "Error: ".$tRetourApiMarque->head->message.'<br />';
	}
	
	
	if(isset($_POST['validSaveProduit']))
	{
		$tParamApiProduit = array();
		$tParamApiProduit['id']				= $_POST['id'];
		$tParamApiProduit['titre']			= $_POST['titre'];
		$tParamApiProduit['description']	= $_POST['description'];
		$tParamApiProduit['prix']			= $_POST['prix'];
		$tParamApiProduit['stock']			= $_POST['stock'];
		$tParamApiProduit['types_id']		= $_POST['types_id'];
		$tParamApiProduit['genre_id']		= $_POST['genre_id'];
		$tParamApiProduit['marque_id']		= $_POST['marque_id'];
		
		
		$tRetourApiSaveProduit = $oApi->fetchData('api-save-produit', $tParamApiProduit);		
		
		if($tRetourApiSaveProduit->head->code == 200)
		{
			echo "Succès: Le produit a été mis à jour<br />";
		}
		else
		{
			echo "Error: ".$tRetourApiSaveProduit->head->message.'<br />';
		}
		//detail produit
		$tParamApiProduit = array("id"=>$iProduitId);
		$tRetourApiProduit = $oApi->fetchData('api-get-produit', $tParamApiProduit);	

		$oProduit = array();
		if($tRetourApiProduit->head->code == 200)
		{
			$oProduit = $tRetourApiProduit->result;
		}
		else
		{
			echo "Error: ".$tRetourApiProduit->head->message.'<br />';
		}
	}

?>
<html>
<body>
<a href="index.php" align="left">Retour vers l'accueil</a>
<?php if(isset($oProduit->id))
{
?>
<form method="post" >
<input type="hidden" name="id" value="<?php echo $oProduit->id;?>"/>
<table border="0">
<tr><td></td><td align="center"><h1>MODIFICATION PRODUIT</h1></tr>
<tr><td>Titre</td><td><input type="text" name="titre" value="<?php echo $oProduit->titre;?>"/></td></tr>
<tr><td>Description</td><td><textarea name="description" ><?php echo $oProduit->description;?></textarea></td></tr>
<tr><td>Prix</td><td><input type="text" name="prix" value="<?php echo $oProduit->prix;?>"/></td></tr>
<tr><td>Stock</td><td><input type="text" name="stock" value="<?php echo $oProduit->stock;?>" /></td></tr>
<tr>
	<td>Types</td>
	<td>
		<select name="types_id">
		<option value="">Choisir le type</option>
		<?php
			if(sizeof($toTypes) > 0)
			{
				foreach($toTypes as $oType)
				{
					
					$zSelected = ($oProduit->types_id == $oType->id)?"selected":"";
					echo '<option value="'.$oType->id.'" '.$zSelected.'>'.$oType->libelle.'</option>';
				}
			}
		?>
		</select>
	</td>
</tr>
<tr>
	<td>Genre</td>
	<td>
		<select name="genre_id">
		<option value="">Choisir le genre</option>
		<?php
			if(sizeof($toGenre) > 0)
			{
				foreach($toGenre as $oGenre)
				{
						$zSelected = ($oProduit->genre_id == $oGenre->id)?"selected":"";
						echo '<option value="'.$oGenre->id.'" '.$zSelected.'>'.$oGenre->libelle.'</option>';
				}
			}
		?>
		</select>
	</td>
</tr>
<tr>
	<td>Marque</td>
	<td>
		<select name="marque_id">
		<option value="">Choisir le genre</option>
		<?php
			if(sizeof($toMarque) > 0)
			{
				foreach($toMarque as $oMarque)
				{
						$zSelected = ($oProduit->marque_id == $oMarque->id)?"selected":"";
						echo '<option value="'.$oMarque->id.'" '.$zSelected.'>'.$oMarque->libelle.'</option>';
				}
			}
		?>
		</select>
	</td>
</tr>
<tr colspan="2"><td><input type="submit" name="validSaveProduit" value="Enregistrer le produit">	</tr>
</table>
</form>
<?php 
}
?>
</body>
</html>