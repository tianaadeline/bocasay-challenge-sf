<?php 
include 'Api.php';
session_start();
$oApi = new Api();

	$iProduitId = (isset($_GET['id']))?$_GET['id']:0;
	
	
	//detail produit
	$tParamApiProduit = array("id"=>$iProduitId);
	$tRetourApiProduit = $oApi->fetchData('api-get-produit', $tParamApiProduit);	

	$oProduit = array();
	if($tRetourApiProduit->head->code == 200)
	{
		$oProduit = $tRetourApiProduit->result;
	}
	else
	{
		echo "Error: ".$tRetourApiProduit->head->message.'<br />';
	}
	//echo "<pre>";print_r($oProduit); echo "</pre>";
	

	if(isset($_POST['validSaveCommande']))
	{
		$tSessionCommande = array();
		if($_SESSION['tCommande'])
		{
			$tSessionCommande = $_SESSION['tCommande'];
		}		
		array_push($tSessionCommande, array(
												"id" => $_POST['id'],
												"quantite"=> $_POST['quantite'],
												"titre"=> $_POST['titre']												
											));
											
		$_SESSION['tCommande'] = $tSessionCommande;
		header("Location:panier.php");
	}

?>
<html>
<body>
<a href="index.php" align="left">Retour vers l'accueil</a>
<?php if(isset($oProduit->id))
{
?>
<form method="post" >
<input type="hidden" name="id" value="<?php echo $oProduit->id;?>"/>
<input type="hidden" name="titre" value="<?php echo $oProduit->titre;?>"/>
<table border="0">
<tr><td></td><td align="center"><h1>MODIFICATION PRODUIT</h1></tr>
<tr><td>Titre</td><td><?php echo $oProduit->titre;?></td></tr>
<tr><td>Description</td><td><?php echo $oProduit->description;?></td></tr>
<tr><td>Prix</td><td><?php echo $oProduit->prix;?></td></tr>
<tr><td>Stock</td><td><?php echo $oProduit->stock;?></td></tr>
<tr><td>Quantité</td><td><input type="text" name="quantite" /></td></tr>
<tr colspan="2"><td><input type="submit" name="validSaveCommande" value="Ajouter au panier">	</tr>
</table>
</form>
<?php 
}
?>
</body>
</html>