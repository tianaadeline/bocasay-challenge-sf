<?php 
include 'Api.php';

$oApi = new Api();


	//liste types
	$tParamApiTypes = array();
	$tRetourApiTypes = $oApi->fetchData('api-get-types', $tParamApiTypes);	

	$toTypes = array();
	if($tRetourApiTypes->head->code == 200)
	{
		$toTypes = $tRetourApiTypes->result;
	}
	else
	{
		echo "Error: ".$tRetourApiTypes->head->message.'<br />';
	}
	
	//liste genre
	$tParamApiGenre = array();
	$tRetourApiGenre = $oApi->fetchData('api-get-genres', $tParamApiGenre);		
	$toGenre = array();
	if($tRetourApiGenre->head->code == 200)
	{
		$toGenre = $tRetourApiGenre->result;
	}
	else
	{
		echo "Error: ".$tRetourApiGenre->head->message.'<br />';
	}
	
	
	//liste marque
	$tParamApiMarque = array();
	$tRetourApiMarque = $oApi->fetchData('api-get-marques', $tParamApiMarque);		
	$toMarque = array();
	if($tRetourApiMarque->head->code == 200)
	{
		$toMarque = $tRetourApiMarque->result;
	}
	else
	{
		echo "Error: ".$tRetourApiMarque->head->message.'<br />';
	}
	
	
	if(isset($_POST['validSaveProduit']))
	{
		$tParamApiProduit = array();
		$tParamApiProduit['titre']			= $_POST['titre'];
		$tParamApiProduit['description']	= $_POST['description'];
		$tParamApiProduit['prix']			= $_POST['prix'];
		$tParamApiProduit['stock']			= $_POST['stock'];
		$tParamApiProduit['types_id']		= $_POST['types_id'];
		$tParamApiProduit['genre_id']		= $_POST['genre_id'];
		$tParamApiProduit['marque_id']		= $_POST['marque_id'];
		
		
		$tRetourApiSaveProduit = $oApi->fetchData('api-save-produit', $tParamApiProduit);		
		
		if($tRetourApiSaveProduit->head->code == 200)
		{
			echo "Succès: Le produit a été ajouté<br />";
		}
		else
		{
			echo "Error: ".$tRetourApiSaveProduit->head->message.'<br />';
		}
	}

?>
<html>
<body>
<a href="index.php" align="left">Retour vers l'accueil</a>
<form method="post" >
<table border="0">
<tr><td></td><td align="center"><h1>AJOUT PRODUIT</h1></tr>
<tr><td>Titre</td><td><input type="text" name="titre"/></td></tr>
<tr><td>Description</td><td><textarea name="description" > </textarea></td></tr>
<tr><td>Prix</td><td><input type="text" name="prix" /></td></tr>
<tr><td>Stock</td><td><input type="text" name="stock" /></td></tr>
<tr>
	<td>Types</td>
	<td>
		<select name="types_id">
		<option value="">Choisir le type</option>
		<?php
			if(sizeof($toTypes) > 0)
			{
				foreach($toTypes as $oType)
				{
						echo '<option value="'.$oType->id.'">'.$oType->libelle.'</option>';
				}
			}
		?>
		</select>
	</td>
</tr>
<tr>
	<td>Genre</td>
	<td>
		<select name="genre_id">
		<option value="">Choisir le genre</option>
		<?php
			if(sizeof($toGenre) > 0)
			{
				foreach($toGenre as $oGenre)
				{
						echo '<option value="'.$oGenre->id.'">'.$oGenre->libelle.'</option>';
				}
			}
		?>
		</select>
	</td>
</tr>
<tr>
	<td>Marque</td>
	<td>
		<select name="marque_id">
		<option value="">Choisir le genre</option>
		<?php
			if(sizeof($toMarque) > 0)
			{
				foreach($toMarque as $oMarque)
				{
						echo '<option value="'.$oMarque->id.'">'.$oMarque->libelle.'</option>';
				}
			}
		?>
		</select>
	</td>
</tr>
<tr colspan="2"><td><input type="submit" name="validSaveProduit" value="Enregistrer le produit">	</tr>
</table>
</form>
</body>
</html>