<?php

class Api
{
	var $zUrlApi;
	

    public function __construct( )
    {
        
		$this->zUrlApi			=	$_SERVER["REQUEST_SCHEME"].'://'.$_SERVER["HTTP_HOST"];
    }

	/**
	* 
	*
	**/
	public function fetchData($_zPointEntree, $_tParams = array())
	{				
		$zParams = '';
		if(sizeof($_tParams))
		{
			foreach($_tParams as $key=>$value)
			{
				$zParams.='&'.$key.'='.urlencode($value);
			}
		}
	
		$zUrl	= $this->zUrlApi.'/'.$_zPointEntree.'?token='.time().$zParams;
		//echo "<pre>";print_r($zUrl); echo "</pre>";
		$oCh = curl_init();
		curl_setopt($oCh, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($oCh, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($oCh, CURLOPT_URL, $zUrl);
		curl_setopt($oCh, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($oCh, CURLOPT_TIMEOUT, 200);
		$sResult = curl_exec($oCh);
		curl_close($oCh);
		$tResult = json_decode( $sResult );
		
		if(!isset($tResult->head->code))
		{
			$tResult =  new \stdclass;
			
			$tHead =  new \stdclass;
			$tHead->code = 500;
			$tHead->message = "Erreur  API";
			$tResult->head = $tHead;
			$tResult->result = null;
		}
		return $tResult;
	}
}

?>