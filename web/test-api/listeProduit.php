<?php 
include 'Api.php';
session_start();
$oApi = new Api();


	//liste produits
	$tParamApiProduit = array();
	$tRetourApiProduit = $oApi->fetchData('api-get-produits', $tParamApiProduit);	

	$toProduits = array();
	if($tRetourApiProduit->head->code == 200)
	{
		$toProduits = $tRetourApiProduit->result;
	}
	else
	{
		echo "Error: ".$tRetourApiProduit->head->message.'<br />';
	}
	
?>
<html>
<body>
<a href="index.php" align="left">Retour vers l'accueil</a>


<table border="0">
<tr colspan="3"> <td align="center"><h1>LES PRODUITS</h1></td></tr>

		<?php
			if(sizeof($toProduits) > 0)
			{
				foreach($toProduits as $oProduit)
				{
						echo '<tr>';
						echo '<td>#'.$oProduit->id.'</td>';
						echo '<td>'.$oProduit->titre.'</td>';
						echo '<td ><a href="modificationProduit.php?id='.$oProduit->id.'">Modifier</a></td>';
						if(isset($_SESSION['client_id']))
						{
							echo '<td ><a href="commanderProduit.php?id='.$oProduit->id.'">Commander</a></td>';
							echo '</tr>';
						}
				}
			}
		?>

</table>

</body>
</html>