<?php 
include 'Api.php';
session_start();
$oApi = new Api();

	if(isset($_POST['validSaveClient']))
	{
		$tParamApiClient = array();
		$tParamApiClient['nom']				= $_POST['nom'];
		$tParamApiClient['prenom']			= $_POST['prenom'];
		$tParamApiClient['adresse']			= $_POST['adresse'];
		$tParamApiClient['email']			= $_POST['email'];
		$tParamApiClient['telephone']		= $_POST['telephone'];		
		
		
		$tRetourApiSaveClient = $oApi->fetchData('api-save-client',$tParamApiClient);		
		
		if($tRetourApiSaveClient->head->code == 200)
		{
			echo "Succès: Inscription terminée<br />";
			$_SESSION['client_id'] = $tRetourApiSaveClient->result->id;
			header("Location:listeProduit.php");
		}
		else
		{
			echo "Error: ".$tRetourApiSaveClient->head->message.'<br />';
		}
	}

?>
<html>
<body>
<a href="index.php" align="left">Retour vers l'accueil</a>
<form method="post" >
<table border="0">
<tr><td></td><td align="center"><h1>INSCRIPTION</h1></tr>
<tr><td>Nom</td><td><input type="text" name="nom"/></td></tr>
<tr><td>Prenom</td><td><input type="text" name="prenom"/></td></tr>
<tr><td>Adresse</td><td><textarea name="adresse" > </textarea></td></tr>
<tr><td>Email</td><td><input type="text" name="email" /></td></tr>
<tr><td>Téléphone</td><td><input type="text" name="telephone" /></td></tr>
<tr>

<tr colspan="2"><td><input type="submit" name="validSaveClient" value="Valider l'inscription">	</tr>
</table>
</form>
</body>
</html>