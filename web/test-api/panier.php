<?php 
include 'Api.php';
session_start();
	$oApi = new Api();
	$tCommande = array();
	if(isset($_SESSION['tCommande']))
	{
		$tCommande = $_SESSION['tCommande'];
	}	

	if(isset($_POST['validSaveCommande']) && isset($_SESSION['client_id']))
	{
		if(sizeof($tCommande))
		{
			
			
			$zCommande = "";
			$tComToString = array();
			foreach($tCommande as $tCom)
			{
					//format: idproduit1_qte1-idproduit12_qte2
					$zLigne = $tCom['id'].'_'.$tCom['quantite'];
					array_push($tComToString,$zLigne);
			}
			if(sizeof($tComToString) > 0)
			{
				$zCommande = implode('-',$tComToString);
			}
			
			
			$tParamApiCommande = array();
			$tParamApiCommande['client_id']	= $_SESSION['client_id'];
			$tParamApiCommande['commande']  = $zCommande;
				
			$tRetourApiSaveCommande = $oApi->fetchData('api-save-commande',$tParamApiCommande);		
			
			if($tRetourApiSaveCommande->head->code == 200)
			{
				echo "Succès: Commande enregistrée<br />";				
				unset($_SESSION['tCommande']);	
				$tCommande = array();
			}
			else
			{
				echo "Error: ".$tRetourApiSaveCommande->head->message.'<br />';
			}
		}
	}
	
?>
<html>
<body>
<a href="index.php" align="left">Retour vers l'accueil</a><br />
<a href="listeProduit.php" align="left">Continuer la commande</a>
<table border="0">
<tr colspan="3"> <td align="center"><h1>LES PRODUITS COMMANDES</h1></td></tr>
<tr>
	<td>#</td>
	<td>Titre</td>
	<td>Quantité</td>
</tr>
		<?php
			if(sizeof($tCommande) > 0)
			{
				foreach($tCommande as $tCom)
				{
						echo '<tr>';
						echo '<td>'.$tCom["id"].'</td>';
						echo '<td>'.$tCom["titre"].'</td>';
						echo '<td>'.$tCom["quantite"].'</td>';
						echo '</tr>';
				}
			}
			else
			{
				echo '<tr>Le panier est vide</tr>';
			}
		?>

</table>
<?php
if(sizeof($tCommande) > 0)
 {
				
?>
	<form method="post" >
	<input type="submit" name="validSaveCommande" value="Valider la commande">
	</form>
<?php
 }			
?>

</body>
</html>