<?php

namespace Challenge\ProduitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Produit
 *
 * @ORM\Table(name="produit")
 * @ORM\Entity(repositoryClass="Challenge\ProduitBundle\Repository\ProduitRepository")
 */
class Produit
{
    /**
     * @var int
     *
     * @ORM\Column(name="produit_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="produit_titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="produit_description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="produit_prixTTC", type="float")
     */
    private $prixTTC;

    /**
     * @var int
     *
     * @ORM\Column(name="produit_stock", type="integer")
     */
    private $stock;
	
		

    /**
     * @var bool
     *
     * @ORM\Column(name="produit_deleted", type="boolean", nullable=true)
     */
    private $deleted;
	
    /**
     * @var \Types
     *
     * @ORM\ManyToOne(targetEntity="Types")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="produit_typesId", referencedColumnName="types_id")
     * })
     */
    private $types;
	
	
    /**
     * @var \Genre
     *
     * @ORM\ManyToOne(targetEntity="Genre")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="produit_genreId", referencedColumnName="genre_id")
     * })
     */
    private $genre;

	
    /**
     * @var \Marque
     *
     * @ORM\ManyToOne(targetEntity="Marque")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="produit_marqueId", referencedColumnName="marque_id")
     * })
     */
    private $marque;
	
	
		
    /**
     * @var Challenge\CommandeBundle\Entity\ProduitCommande
     * @ORM\OneToMany(targetEntity="Challenge\CommandeBundle\Entity\ProduitCommande", mappedBy="produit")
     * @ORM\JoinTable(name="produitCommande")
     */
    protected $produitCommande;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Produit
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    
        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Produit
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set prixTTC
     *
     * @param float $prixTTC
     *
     * @return Produit
     */
    public function setPrixTTC($prixTTC)
    {
        $this->prixTTC = $prixTTC;
    
        return $this;
    }

    /**
     * Get prixTTC
     *
     * @return float
     */
    public function getPrixTTC()
    {
        return $this->prixTTC;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     *
     * @return Produit
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    
        return $this;
    }

    /**
     * Get stock
     *
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }
	
	
		
	/**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Produit
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
	
	
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->produitCommande = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set types
     *
     * @param \Challenge\ProduitBundle\Entity\Types $types
     *
     * @return Produit
     */
    public function setTypes(\Challenge\ProduitBundle\Entity\Types $types = null)
    {
        $this->types = $types;
    
        return $this;
    }

    /**
     * Get types
     *
     * @return \Challenge\ProduitBundle\Entity\Types
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * Set genre
     *
     * @param \Challenge\ProduitBundle\Entity\Genre $genre
     *
     * @return Produit
     */
    public function setGenre(\Challenge\ProduitBundle\Entity\Genre $genre = null)
    {
        $this->genre = $genre;
    
        return $this;
    }

    /**
     * Get genre
     *
     * @return \Challenge\ProduitBundle\Entity\Genre
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * Set marque
     *
     * @param \Challenge\ProduitBundle\Entity\Marque $marque
     *
     * @return Produit
     */
    public function setMarque(\Challenge\ProduitBundle\Entity\Marque $marque = null)
    {
        $this->marque = $marque;
    
        return $this;
    }

    /**
     * Get marque
     *
     * @return \Challenge\ProduitBundle\Entity\Marque
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * Add produitCommande
     *
     * @param \Challenge\CommandeBundle\Entity\ProduitCommande $produitCommande
     *
     * @return Produit
     */
    public function addProduitCommande(\Challenge\CommandeBundle\Entity\ProduitCommande $produitCommande)
    {
        $this->produitCommande[] = $produitCommande;
    
        return $this;
    }

    /**
     * Remove produitCommande
     *
     * @param \Challenge\CommandeBundle\Entity\ProduitCommande $produitCommande
     */
    public function removeProduitCommande(\Challenge\CommandeBundle\Entity\ProduitCommande $produitCommande)
    {
        $this->produitCommande->removeElement($produitCommande);
    }

    /**
     * Get produitCommande
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduitCommande()
    {
        return $this->produitCommande;
    }

}
