<?php

namespace Challenge\ProduitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Marque
 *
 * @ORM\Table(name="marque")
 * @ORM\Entity(repositoryClass="Challenge\ProduitBundle\Repository\MarqueRepository")
 */
class Marque
{
    /**
     * @var int
     *
     * @ORM\Column(name="marque_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="marque_libelle", type="string", length=255)
     */
    private $libelle;


    /**
     * @var bool
     *
     * @ORM\Column(name="marque_deleted", type="boolean", nullable=true)
     */
    private $deleted;
	
    /**
     * @var Challenge\ProduitBundle\Entity\Produit
     * @ORM\OneToMany(targetEntity="Challenge\ProduitBundle\Entity\Produit", mappedBy="marque")
     * @ORM\JoinTable(name="produit")
     */
    protected $produit;
	
	
	
	
    /**
     * @var \Fournisseur
     *
     * @ORM\ManyToOne(targetEntity="Fournisseur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="marque_fournisseurId", referencedColumnName="fournisseur_id")
     * })
     */
    private $fournisseur;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Marque
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    
        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
	
	
		
	
	/**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Marque
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
	
	
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->produit = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add produit
     *
     * @param \Challenge\ProduitBundle\Entity\Produit $produit
     *
     * @return Marque
     */
    public function addProduit(\Challenge\ProduitBundle\Entity\Produit $produit)
    {
        $this->produit[] = $produit;
    
        return $this;
    }

    /**
     * Remove produit
     *
     * @param \Challenge\ProduitBundle\Entity\Produit $produit
     */
    public function removeProduit(\Challenge\ProduitBundle\Entity\Produit $produit)
    {
        $this->produit->removeElement($produit);
    }

    /**
     * Get produit
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Set fournisseur
     *
     * @param \Challenge\ProduitBundle\Entity\Fournisseur $fournisseur
     *
     * @return Marque
     */
    public function setFournisseur(\Challenge\ProduitBundle\Entity\Fournisseur $fournisseur = null)
    {
        $this->fournisseur = $fournisseur;
    
        return $this;
    }

    /**
     * Get fournisseur
     *
     * @return \Challenge\ProduitBundle\Entity\Fournisseur
     */
    public function getFournisseur()
    {
        return $this->fournisseur;
    }
}
