<?php

namespace Challenge\ProduitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Types
 *
 * @ORM\Table(name="types")
 * @ORM\Entity(repositoryClass="Challenge\ProduitBundle\Repository\TypesRepository")
 */
class Types
{
    /**
     * @var int
     *
     * @ORM\Column(name="types_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="types_libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var bool
     *
     * @ORM\Column(name="types_deleted", type="boolean", nullable=true)
     */
    private $deleted;
	
    /**
     * @var Challenge\ProduitBundle\Entity\Produit
     * @ORM\OneToMany(targetEntity="Challenge\ProduitBundle\Entity\Produit", mappedBy="types")
     * @ORM\JoinTable(name="produit")
     */
    protected $produit;
	
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Types
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    
        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
	
	
	
	/**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Types
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
	
	
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->produit = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add produit
     *
     * @param \Challenge\ProduitBundle\Entity\Produit $produit
     *
     * @return Types
     */
    public function addProduit(\Challenge\ProduitBundle\Entity\Produit $produit)
    {
        $this->produit[] = $produit;
    
        return $this;
    }

    /**
     * Remove produit
     *
     * @param \Challenge\ProduitBundle\Entity\Produit $produit
     */
    public function removeProduit(\Challenge\ProduitBundle\Entity\Produit $produit)
    {
        $this->produit->removeElement($produit);
    }

    /**
     * Get produit
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduit()
    {
        return $this->produit;
    }
}
