<?php

namespace Challenge\ProduitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fournisseur
 *
 * @ORM\Table(name="fournisseur")
 * @ORM\Entity(repositoryClass="Challenge\ProduitBundle\Repository\FournisseurRepository")
 */
class Fournisseur
{
    /**
     * @var int
     *
     * @ORM\Column(name="fournisseur_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fournisseur_nom", type="string", length=255)
     */
    private $nom;


    /**
     * @var bool
     *
     * @ORM\Column(name="fournisseur_deleted", type="boolean", nullable=true)
     */
    private $deleted;
	
	
    /**
     * @var Challenge\MarqueBundle\Entity\Marque
     * @ORM\OneToMany(targetEntity="Challenge\ProduitBundle\Entity\Marque", mappedBy="fournisseur")
     * @ORM\JoinTable(name="marque")
     */
    protected $marque;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Fournisseur
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
	
	
		
	/**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Fournisseur
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
	
	
	
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->marque = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add marque
     *
     * @param \Challenge\ProduitBundle\Entity\Marque $marque
     *
     * @return Fournisseur
     */
    public function addMarque(\Challenge\ProduitBundle\Entity\Marque $marque)
    {
        $this->marque[] = $marque;
    
        return $this;
    }

    /**
     * Remove marque
     *
     * @param \Challenge\ProduitBundle\Entity\Marque $marque
     */
    public function removeMarque(\Challenge\ProduitBundle\Entity\Marque $marque)
    {
        $this->marque->removeElement($marque);
    }

    /**
     * Get marque
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMarque()
    {
        return $this->marque;
    }
}
