<?php

namespace Challenge\ProduitBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Challenge\CommandeBundle\Entity\Commande;

class AdminCommandeController extends Controller
{
    public function listeCommandeAction(Request $oRequest)
    {
		$oContainer  	= $this->container;
		$oEm         	= $this->getDoctrine()->getManager();		
		$oRepCommande 	= $oEm->getRepository('ChallengeCommandeBundle:Commande');
		$aoCommande   	= $oRepCommande->getListCommande();
		$aoAssign    	= array( 
							'aoCommande'   => $aoCommande
						); 

		if($oRequest->isXmlHttpRequest()) {
			return $this->render( '@ChallengeProduit/Admin/Page/tableCommande.html.twig', $aoAssign );
        } else {
            return $this->render( '@ChallengeProduit/Admin/Page/listeCommande.html.twig', $aoAssign );
        }
        
    }
	
	public function detailCommandeAction($_iCommandeId, Request $oRequest)
    {
		$oContainer  	= $this->container;
		$oEm         	= $this->getDoctrine()->getManager();		
		$oRepCommande 	= $oEm->getRepository('ChallengeCommandeBundle:Commande');
		$oCommande   	= $oRepCommande->find($_iCommandeId);
		$aoAssign    	= array( 
							'oCommande'   => $oCommande
						); 

		if(!is_object($oCommande )) {
			return $this->render( '@ChallengeProduit/Admin/Page/listeCommande.html.twig', $aoAssign );
        } else {
            return $this->render( '@ChallengeProduit/Admin/Page/detailCommande.html.twig', $aoAssign );
        }
        
    }
}
