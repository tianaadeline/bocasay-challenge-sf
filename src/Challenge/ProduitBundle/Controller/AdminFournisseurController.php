<?php

namespace Challenge\ProduitBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Challenge\ProduitBundle\Entity\Fournisseur;
class AdminFournisseurController extends Controller
{
    public function listeFournisseurAction(Request $oRequest)
    {
		$oContainer  		= $this->container;
		$oEm         		= $this->getDoctrine()->getManager();		
		$oRepFournisseur	= $oEm->getRepository('ChallengeProduitBundle:Fournisseur');
		$aoFournisseur     	= $oRepFournisseur->getListFournisseur();
		$aoAssign    		= array( 
								'aoFournisseur'   => $aoFournisseur
							); 

		if($oRequest->isXmlHttpRequest()) {
			return $this->render( '@ChallengeProduit/Admin/Page/tableFournisseur.html.twig', $aoAssign );
        } else {
            return $this->render( '@ChallengeProduit/Admin/Page/listeFournisseur.html.twig', $aoAssign );
        }
        
    }
	
	/**
     * Suppression fournisseur
     * @param  Request $oRequest
     * @return
     */
    public function deleteAction( Request $oRequest )
    {
		$iFournisseurId		= $oRequest->get('iFournisseur', 0);
		$oEm				= $this->getDoctrine()->getManager();		
		$oRepFournisseur 	= $oEm->getRepository('ChallengeProduitBundle:Fournisseur');
		$aoRes      	= array();

		if( $iFournisseurId > 0 ){
			$oFournisseur = $oRepFournisseur->find($iFournisseurId);
			if( is_object($oFournisseur) ){
				$iUpdated = $oRepFournisseur->deleteFournisseur($oFournisseur);
				if( $iUpdated > 0 ){
					$this->get('session')->getFlashBag()->set('success', 'Le fournisseur a été supprimé avec succès !');
					$aoRes = array( 'success' => 1, 'msg' => 'Le fournisseur a été supprimé avec succès !');
				}else{
					$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de la suppression !');
					$aoRes = array( 'success' => 0, 'msg' => 'Une erreur s\'est produite lors de la suppression !');					
				}
			}
			else{
				$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de la suppression !');
				$aoRes = array( 'success' => 0, 'msg' => 'Une erreur s\'est produite lors de la suppression !');
			}
		}
		else{
			$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de la suppression !');
			$aoRes = array( 'success' => 0, 'msg' => 'Une erreur s\'est produite lors de la suppression !');
		}
		return new Response (json_encode( $aoRes ), 200, array ('Content-Type' => 'application/json')) ;
    }
	
	
	
	/**
	 * edit or add fournisseur
	 * @param integer $_iFournisseurId
	 * @param Request $oRequest
	 */
    public function addAction($_iFournisseurId, Request $oRequest)
    {
		$iFournisseurId		= intval($_iFournisseurId);
		$oEm				= $this->getDoctrine()->getManager();		
		$oRepFournisseur	= $oEm->getRepository('ChallengeProduitBundle:Fournisseur');		
		$oFournisseur		= $oRepFournisseur->find($iFournisseurId);		

		$aoAssign		= array( 
								'oFournisseur'		=> $oFournisseur
						);
		return $this->render('@ChallengeProduit/Admin/Page/addFournisseur.html.twig', $aoAssign);
    }
	
	
	/**
	 * save or update fournisseur
	 * @param  Request $oRequest
	 * @return 
	 */
    public function saveAction(Request $oRequest){

		$oEm							= $this->getDoctrine()->getManager();	
		$tFournisseur					= array();
		$tFournisseur['iFournisseurId']	= intval( $oRequest->get('_iFournisseurId') );
		$tFournisseur['zNom']			= $oRequest->get('nom');
	
				
		$oRepFournisseur    = $oEm->getRepository('ChallengeProduitBundle:Fournisseur');			
		$oFournisseur       = $oRepFournisseur->saveFournisseur($tFournisseur);
		$iRedirectId = 0;
		

		if( $oFournisseur->getId() > 0 ){
			$this->get('session')->getFlashBag()->set('success', 'Enregistrement effectué avec succès !');
			$iRedirectId = $oFournisseur->getId();
		}else{
			$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de l\'enregistrement !');
		}
		return $this->redirectToRoute('challenge_fournisseur_ajout', array( '_iFournisseurId' => $iRedirectId ));
    }
}
