<?php

namespace Challenge\ProduitBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Challenge\ProduitBundle\Entity\Genre;
class AdminGenreController extends Controller
{
    public function listeGenreAction(Request $oRequest)
    {
		$oContainer  = $this->container;
		$oEm         = $this->getDoctrine()->getManager();		
		$oRepGenre 	 = $oEm->getRepository('ChallengeProduitBundle:Genre');
		$aoGenre     = $oRepGenre->getListGenre();
		$aoAssign    = array( 
							'aoGenre'   => $aoGenre
					); 

		if($oRequest->isXmlHttpRequest()) {
			return $this->render( '@ChallengeProduit/Admin/Page/tableGenre.html.twig', $aoAssign );
        } else {
            return $this->render( '@ChallengeProduit/Admin/Page/listeGenre.html.twig', $aoAssign );
        }
        
    }
	
	/**
     * Suppression genre
     * @param  Request $oRequest
     * @return
     */
    public function deleteAction( Request $oRequest )
    {
		$iGenreId		= $oRequest->get('iGenre', 0);
		$oEm			= $this->getDoctrine()->getManager();		
		$oRepGenre 		= $oEm->getRepository('ChallengeProduitBundle:Genre');
		$aoRes      	= array();

		if( $iGenreId > 0 ){
			$oGenre = $oRepGenre->find($iGenreId);
			if( is_object($oGenre) ){
				$iUpdated = $oRepGenre->deleteGenre($oGenre);
				if( $iUpdated > 0 ){
					$this->get('session')->getFlashBag()->set('success', 'Le genre a été supprimé avec succès !');
					$aoRes = array( 'success' => 1, 'msg' => 'Le genre a été supprimé avec succès !');
				}else{
					$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de la suppression !');
					$aoRes = array( 'success' => 0, 'msg' => 'Une erreur s\'est produite lors de la suppression !');					
				}
			}
			else{
				$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de la suppression !');
				$aoRes = array( 'success' => 0, 'msg' => 'Une erreur s\'est produite lors de la suppression !');
			}
		}
		else{
			$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de la suppression !');
			$aoRes = array( 'success' => 0, 'msg' => 'Une erreur s\'est produite lors de la suppression !');
		}
		return new Response (json_encode( $aoRes ), 200, array ('Content-Type' => 'application/json')) ;
    }
	
	
	
	/**
	 * edit or add genre
	 * @param integer $_iGenreId
	 * @param Request $oRequest
	 */
    public function addAction($_iGenreId, Request $oRequest)
    {
		$iGenreId		= intval($_iGenreId);
		$oEm			= $this->getDoctrine()->getManager();		
		$oRepGenre		= $oEm->getRepository('ChallengeProduitBundle:Genre');		
		$oGenre			= $oRepGenre->find($iGenreId);		
		
		
		
		
		$aoAssign		= array( 
								'oGenre'		=> $oGenre
						);
		return $this->render('@ChallengeProduit/Admin/Page/addGenre.html.twig', $aoAssign);
    }
	
	
	/**
	 * save or update genre
	 * @param  Request $oRequest
	 * @return 
	 */
    public function saveAction(Request $oRequest){

		$oEm					= $this->getDoctrine()->getManager();	
		$tGenre					= array();
		$tGenre['iGenreId']		= intval( $oRequest->get('_iGenreId') );
		$tGenre['zLibelle']		= $oRequest->get('libelle');
	
				
		$oRepGenre    = $oEm->getRepository('ChallengeProduitBundle:Genre');			
		$oGenre       = $oRepGenre->saveGenre($tGenre);
		$iRedirectId = 0;
		

		if( $oGenre->getId() > 0 ){
			$this->get('session')->getFlashBag()->set('success', 'Enregistrement effectué avec succès !');
			$iRedirectId = $oGenre->getId();
		}else{
			$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de l\'enregistrement !');
		}
		return $this->redirectToRoute('challenge_genre_ajout', array( '_iGenreId' => $iRedirectId ));
    }
}
