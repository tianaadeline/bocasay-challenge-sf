<?php

namespace Challenge\ProduitBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Challenge\ProduitBundle\Entity\Marque;
use Challenge\ProduitBundle\Entity\Fournisseur;
class AdminMarqueController extends Controller
{
    public function listeMarqueAction(Request $oRequest)
    {
		$oContainer  		= $this->container;
		$oEm         		= $this->getDoctrine()->getManager();		
		$oRepMarque			= $oEm->getRepository('ChallengeProduitBundle:Marque');
		$aoMarque     		= $oRepMarque->getListMarque();
		$aoAssign    		= array( 
								'aoMarque'   => $aoMarque
							); 

		if($oRequest->isXmlHttpRequest()) {
			return $this->render( '@ChallengeProduit/Admin/Page/tableMarque.html.twig', $aoAssign );
        } else {
            return $this->render( '@ChallengeProduit/Admin/Page/listeMarque.html.twig', $aoAssign );
        }
        
    }
	
	/**
     * Suppression Marque
     * @param  Request $oRequest
     * @return
     */
    public function deleteAction( Request $oRequest )
    {
		$iMarqueId			= $oRequest->get('iMarque', 0);
		$oEm				= $this->getDoctrine()->getManager();		
		$oRepMarque 		= $oEm->getRepository('ChallengeProduitBundle:Marque');
		$aoRes      	= array();

		if( $iMarqueId > 0 ){
			$oMarque = $oRepMarque->find($iMarqueId);
			if( is_object($oMarque) ){
				$iUpdated = $oRepMarque->deleteMarque($oMarque);
				if( $iUpdated > 0 ){
					$this->get('session')->getFlashBag()->set('success', 'La marque a été supprimé avec succès !');
					$aoRes = array( 'success' => 1, 'msg' => 'La marque a été supprimé avec succès !');
				}else{
					$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de la suppression !');
					$aoRes = array( 'success' => 0, 'msg' => 'Une erreur s\'est produite lors de la suppression !');					
				}
			}
			else{
				$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de la suppression !');
				$aoRes = array( 'success' => 0, 'msg' => 'Une erreur s\'est produite lors de la suppression !');
			}
		}
		else{
			$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de la suppression !');
			$aoRes = array( 'success' => 0, 'msg' => 'Une erreur s\'est produite lors de la suppression !');
		}
		return new Response (json_encode( $aoRes ), 200, array ('Content-Type' => 'application/json')) ;
    }
	
	
	
	/**
	 * edit or add Marque
	 * @param integer $_iMarqueId
	 * @param Request $oRequest
	 */
    public function addAction($_iMarqueId, Request $oRequest)
    {
		$iMarqueId			= intval($_iMarqueId);
		$oEm				= $this->getDoctrine()->getManager();		
		$oRepMarque			= $oEm->getRepository('ChallengeProduitBundle:Marque');		
		$oMarque			= $oRepMarque->find($iMarqueId);		

		
		$oRepFournisseur	= $oEm->getRepository('ChallengeProduitBundle:Fournisseur');	
		$aoFournisseur		= $oRepFournisseur->getListFournisseur();
		
		
		
		$aoAssign		= array( 
								'oMarque'		=> $oMarque,
								'aoFournisseur' => $aoFournisseur
						);
		return $this->render('@ChallengeProduit/Admin/Page/addMarque.html.twig', $aoAssign);
    }
	
	
	/**
	 * save or update marque
	 * @param  Request $oRequest
	 * @return 
	 */
    public function saveAction(Request $oRequest){

		$oEm							= $this->getDoctrine()->getManager();	
		$tMarque						= array();
		$tMarque['iMarqueId']			= intval( $oRequest->get('_iMarqueId') );
		$tMarque['zLibelle']			= $oRequest->get('libelle');
		$tMarque['iFournisseurId']		= intval( $oRequest->get('fournisseurId') );
	
				
		$oRepMarque    = $oEm->getRepository('ChallengeProduitBundle:Marque');			
		$oMarque       = $oRepMarque->saveMarque($tMarque);
		$iRedirectId = 0;
		

		if( $oMarque->getId() > 0 ){
			$this->get('session')->getFlashBag()->set('success', 'Enregistrement effectué avec succès !');
			$iRedirectId = $oMarque->getId();
		}else{
			$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de l\'enregistrement !');
		}
		return $this->redirectToRoute('challenge_marque_ajout', array( '_iMarqueId' => $iRedirectId ));
    }
}
