<?php

namespace Challenge\ProduitBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Challenge\ProduitBundle\Entity\Types;
class AdminTypesController extends Controller
{
    public function listeTypesAction(Request $oRequest)
    {
		$oContainer  = $this->container;
		$oEm         = $this->getDoctrine()->getManager();		
		$oRepTypes 	 = $oEm->getRepository('ChallengeProduitBundle:Types');
		$aoTypes     = $oRepTypes->getListTypes();
		$aoAssign    = array( 
							'aoTypes'   => $aoTypes
					); 

		if($oRequest->isXmlHttpRequest()) {
			return $this->render( '@ChallengeProduit/Admin/Page/tableTypes.html.twig', $aoAssign );
        } else {
            return $this->render( '@ChallengeProduit/Admin/Page/listeTypes.html.twig', $aoAssign );
        }
        
    }
	
	/**
     * Suppression types
     * @param  Request $oRequest
     * @return
     */
    public function deleteAction( Request $oRequest )
    {
		$iTypesId		= $oRequest->get('iTypes', 0);
		$oEm			= $this->getDoctrine()->getManager();		
		$oRepTypes 		= $oEm->getRepository('ChallengeProduitBundle:Types');
		$aoRes      	= array();

		if( $iTypesId > 0 ){
			$oTypes = $oRepTypes->find($iTypesId);
			if( is_object($oTypes) ){
				$iUpdated = $oRepTypes->deleteTypes($oTypes);
				if( $iUpdated > 0 ){
					$this->get('session')->getFlashBag()->set('success', 'Le type a été supprimé avec succès !');
					$aoRes = array( 'success' => 1, 'msg' => 'Le type a été supprimé avec succès !');
				}else{
					$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de la suppression !');
					$aoRes = array( 'success' => 0, 'msg' => 'Une erreur s\'est produite lors de la suppression !');					
				}
			}
			else{
				$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de la suppression !');
				$aoRes = array( 'success' => 0, 'msg' => 'Une erreur s\'est produite lors de la suppression !');
			}
		}
		else{
			$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de la suppression !');
			$aoRes = array( 'success' => 0, 'msg' => 'Une erreur s\'est produite lors de la suppression !');
		}
		return new Response (json_encode( $aoRes ), 200, array ('Content-Type' => 'application/json')) ;
    }
	
	
	
	/**
	 * edit or add types
	 * @param integer $_iTypesId
	 * @param Request $oRequest
	 */
    public function addAction($_iTypesId, Request $oRequest)
    {
		$iTypesId		= intval($_iTypesId);
		$oEm			= $this->getDoctrine()->getManager();		
		$oRepTypes		= $oEm->getRepository('ChallengeProduitBundle:Types');		
		$oTypes			= $oRepTypes->find($iTypesId);		
		
		
		
		
		$aoAssign		= array( 
								'oTypes'		=> $oTypes
						);
		return $this->render('@ChallengeProduit/Admin/Page/addTypes.html.twig', $aoAssign);
    }
	
	
	/**
	 * save or update types
	 * @param  Request $oRequest
	 * @return 
	 */
    public function saveAction(Request $oRequest){

		$oEm					= $this->getDoctrine()->getManager();	
		$tTypes					= array();
		$tTypes['iTypesId']		= intval( $oRequest->get('_iTypesId') );
		$tTypes['zLibelle']		= $oRequest->get('libelle');
	
				
		$oRepTypes    = $oEm->getRepository('ChallengeProduitBundle:Types');			
		$oTypes       = $oRepTypes->saveTypes($tTypes);
		$iRedirectId = 0;
		

		if( $oTypes->getId() > 0 ){
			$this->get('session')->getFlashBag()->set('success', 'Enregistrement effectué avec succès !');
			$iRedirectId = $oTypes->getId();
		}else{
			$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de l\'enregistrement !');
		}
		return $this->redirectToRoute('challenge_types_ajout', array( '_iTypesId' => $iRedirectId ));
    }
}
