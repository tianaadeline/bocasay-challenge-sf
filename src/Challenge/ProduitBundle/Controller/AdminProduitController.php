<?php

namespace Challenge\ProduitBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Challenge\ProduitBundle\Entity\Produit;
use Challenge\ProduitBundle\Entity\Types;
use Challenge\ProduitBundle\Entity\Genre;
use Challenge\ProduitBundle\Entity\Marque;
class AdminProduitController extends Controller
{
    public function listeProduitAction(Request $oRequest)
    {
		$oContainer  = $this->container;
		$oEm         = $this->getDoctrine()->getManager();		
		$oRepProduit = $oEm->getRepository('ChallengeProduitBundle:Produit');
		$aoProduit   = $oRepProduit->getListProduit();
		$aoAssign    = array( 
							'aoProduit'   => $aoProduit
					); 

		if($oRequest->isXmlHttpRequest()) {
			return $this->render( '@ChallengeProduit/Admin/Page/tableProduit.html.twig', $aoAssign );
        } else {
            return $this->render( '@ChallengeProduit/Admin/Page/listeProduit.html.twig', $aoAssign );
        }
        
    }
	
	/**
     * Suppression produit
     * @param  Request $oRequest
     * @return
     */
    public function deleteAction( Request $oRequest )
    {
		$iProduitId		= $oRequest->get('iProduit', 0);
		$oEm			= $this->getDoctrine()->getManager();		
		$oRepProduit 	= $oEm->getRepository('ChallengeProduitBundle:Produit');
		$aoRes      	= array();

		if( $iProduitId > 0 ){
			$oProduit = $oRepProduit->find($iProduitId);
			if( is_object($oProduit) ){
				$iUpdated = $oRepProduit->deleteProduit($oProduit);
				if( $iUpdated > 0 ){
					$this->get('session')->getFlashBag()->set('success', 'Le produit a été supprimé avec succès !');
					$aoRes = array( 'success' => 1, 'msg' => 'Le produit a été supprimé avec succès !');
				}else{
					$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de la suppression !');
					$aoRes = array( 'success' => 0, 'msg' => 'Une erreur s\'est produite lors de la suppression !');					
				}
			}
			else{
				$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de la suppression !');
				$aoRes = array( 'success' => 0, 'msg' => 'Une erreur s\'est produite lors de la suppression !');
			}
		}
		else{
			$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de la suppression !');
			$aoRes = array( 'success' => 0, 'msg' => 'Une erreur s\'est produite lors de la suppression !');
		}
		return new Response (json_encode( $aoRes ), 200, array ('Content-Type' => 'application/json')) ;
    }
	
	
	
	/**
	 * edit or add produit
	 * @param integer $_iProduitId
	 * @param Request $oRequest
	 */
    public function addAction($_iProduitId, Request $oRequest)
    {
		$iProduitId		= intval($_iProduitId);
		$oEm			= $this->getDoctrine()->getManager();		
		$oRepProduit	= $oEm->getRepository('ChallengeProduitBundle:Produit');		
		$oProduit		= $oRepProduit->find($iProduitId);		
		
		$oRepTypes		= $oEm->getRepository('ChallengeProduitBundle:Types');	
		$aoTypes		= $oRepTypes->getListTypes();

		$oRepGenre		= $oEm->getRepository('ChallengeProduitBundle:Genre');	
		$aoGenre		= $oRepGenre->getListGenre();
		
		$oRepMarque		= $oEm->getRepository('ChallengeProduitBundle:Marque');	
		$aoMarque		= $oRepMarque->getListMarque();
		
		
		$aoAssign		= array( 
								'oProduit'		=> $oProduit,
								'aoTypes'		=> $aoTypes,
								'aoGenre'		=> $aoGenre,
								'aoMarque'		=> $aoMarque
						);
		return $this->render('@ChallengeProduit/Admin/Page/addProduit.html.twig', $aoAssign);
    }
	
	
	/**
	 * save or update produit
	 * @param  Request $oRequest
	 * @return 
	 */
    public function saveAction(Request $oRequest){

		$oEm						= $this->getDoctrine()->getManager();	
		$tProduit					= array();
		$tProduit['iProduitId']		= intval( $oRequest->get('_iProduitId') );
		$tProduit['zTitre']			= $oRequest->get('titre');
		$tProduit['zDescription']	= $oRequest->get('description');
		$tProduit['fPrixTTC']		= $oRequest->get('prixTTC');
		$tProduit['iStock']			= $oRequest->get('stock');
		$tProduit['iTypesId']		= $oRequest->get('typesId');
		$tProduit['iGenreId']		= $oRequest->get('genreId');
		$tProduit['iMarqueId']		= $oRequest->get('marqueId');
		
		
		
				
		$oRepProduit    = $oEm->getRepository('ChallengeProduitBundle:Produit');			
		$oProduit      	= $oRepProduit->saveProduit($tProduit);
		$iRedirectId = 0;
		

		if( $oProduit->getId() > 0 ){
			$this->get('session')->getFlashBag()->set('success', 'Enregistrement effectué avec succès !');
			$iRedirectId = $oProduit->getId();
		}else{
			$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de l\'enregistrement !');
		}
		return $this->redirectToRoute('challenge_produit_ajout', array( '_iProduitId' => $iRedirectId ));
    }
}
