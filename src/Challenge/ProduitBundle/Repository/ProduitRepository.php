<?php

namespace Challenge\ProduitBundle\Repository;
use Challenge\ProduitBundle\Entity\Produit;
/**
 * ProduitRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProduitRepository extends \Doctrine\ORM\EntityRepository
{
	
	/**
	 * get list produit active
	 * @param 
	 * @return array object
	 */
	public function getListProduit(){
        $sQuery = $this->createQueryBuilder('p');
		$sQuery->join('p.genre','g');
		$sQuery->join('p.types','t');
		$sQuery->join('p.marque','m');
		$sQuery->join('m.fournisseur','f');
        $sQuery->where("p.deleted is null OR p.deleted <> 1") ;
		$sQuery->andWhere("m.deleted is null OR m.deleted <> 1") ;
		$sQuery->andWhere("f.deleted is null OR f.deleted <> 1") ;
		$sQuery->andWhere("t.deleted is null OR t.deleted <> 1") ;
		$sQuery->andWhere("g.deleted is null OR g.deleted <> 1") ;
		$sQuery->orderBy("p.id","asc") ;
        $oQuery     = $sQuery->getQuery();
        $aoProduits   = $oQuery->getResult();        
        if( sizeof( $aoProduits ) > 0 ){
            return $aoProduits;
        }else{
            return false;
        }
	}
	
	
	/**
	 * set produit deleted
	 * @param  object $_oProduit
	 * @return       
	 */
	public function deleteProduit($_oProduit){
		$oEm = $this->getEntityManager();
		$_oProduit->setDeleted(1);
		$oEm->persist($_oProduit);
		$oEm->flush();
		return $_oProduit->getId();
	}
	
	/**
	 * set produit save or update
	 * @param array $_tProduit
	 * @return object         
	 */
	public function saveProduit($_tProduit){
		$oEm 			= $this->getEntityManager();
		$oRepProduit    = $oEm->getRepository('ChallengeProduitBundle:Produit');	
		$oRepTypes		= $oEm->getRepository('ChallengeProduitBundle:Types');
		$oRepGenre		= $oEm->getRepository('ChallengeProduitBundle:Genre');
		$oRepMarque		= $oEm->getRepository('ChallengeProduitBundle:Marque');
		
		$oProduit      	= $oRepProduit->find($_tProduit['iProduitId']);
		if( !is_object($oProduit) ){
			$oProduit = new Produit();
		}			
		$oProduit->setTitre( $_tProduit['zTitre'] );
		$oProduit->setDescription( $_tProduit['zDescription'] );
		$oProduit->setPrixTTC((float)$_tProduit['fPrixTTC']);
		$oProduit->setStock((int)$_tProduit['iStock']);
		
		$oTypes = $oRepTypes->find($_tProduit['iTypesId']);
		if(is_object($oTypes))
		{
			$oProduit->setTypes($oTypes);
		}
		
		$oGenre = $oRepGenre->find($_tProduit['iGenreId']);
		if(is_object($oGenre))
		{
			$oProduit->setGenre($oGenre);
		}
		
		$oMarque = $oRepMarque->find($_tProduit['iMarqueId']);
		if(is_object($oMarque))
		{
			$oProduit->setMarque($oMarque);
		}
		
		$oEm->persist($oProduit);
		$oEm->flush();
		
		return $oProduit;
	}
		
}
