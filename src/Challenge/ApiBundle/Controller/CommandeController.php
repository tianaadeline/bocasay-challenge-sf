<?php

namespace Challenge\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Challenge\ApiBundle\Controller\ApiController as ApiController;
class CommandeController extends ApiController
{

	/*
	* Point d'entrée créer client
	*/
	public function saveClientAction(Request $oRequest){

		$iCode 	= 200;
        $zMessage = null;
		$tResult  = array();
		$tErreur = array();
		
		$oEm     			    	= $this->container->get('doctrine')->getEntityManager();
		$oContainer     	    	= $this->container;		
		
		$tClient					= array();
		$tClient['zNom']			= $oRequest->get('nom');
		$tClient['zPrenom']			= $oRequest->get('prenom');
		$tClient['zAdresse']		= $oRequest->get('adresse');
		$tClient['zEmail']			= $oRequest->get('email');
		$tClient['iTelephone']		= $oRequest->get('telephone');
		

		if($tClient['zNom']== '')
		{
			array_push($tErreur,"Le nom est obligatoire");
		}
			
		if($tClient['zPrenom']== '' )
		{
			array_push($tErreur,"Le prénom est obligatoire");
		}
		
		if($tClient['zAdresse']== '' )
		{
			array_push($tErreur,"L'adresse est obligatoire");
		}
		
		if($tClient['zEmail']== '' )
		{
			array_push($tErreur,"L'email est obligatoire");
		}
		
		if($tClient['iTelephone']== '' )
		{
			array_push($tErreur,"Le téléphone est obligatoire");
		}
				
		if(sizeof($tErreur) > 0)
		{
			$zMessage =  'Une erreur s\'est produite lors de l\'enregistrement :' ;
			foreach($tErreur as $zErreur)
			{
				$zMessage.=' - '.$zErreur;
			}
		}
		else
		{						
				$oRepClient 	= $oEm->getRepository('ChallengeCommandeBundle:Client');		
				$oClient      	= $oRepClient->saveClient($tClient);
			
			if( $oClient->getId() > 0 )
			{
				$zMessage =  'Inscription effectué avec succès !';
				$tResult['id'] = $oClient->getId();
			}else
			{
				$iCode 	= 403;
				$zMessage =  'Une erreur s\'est produite lors de l\'enregistrement !' ;
			}
			
		}
	
		parent::setCode($iCode);
		parent::setMessage($zMessage);
		parent::setResults($tResult);
		$zResult  = parent::getFormatedResults();
		return new Response( $zResult, 200, array ('Content-Type' => 'application/json'));
	}
	
	
	
	/*
	* Point d'entrée créer commande
	*/
	public function saveCommandeAction(Request $oRequest){

		$iCode 	= 200;
        $zMessage = null;
		$tResult  = array();
		$tErreur = array();
		
		$oEm     			    	= $this->container->get('doctrine')->getEntityManager();
		$oRepClient 				= $oEm->getRepository('ChallengeCommandeBundle:Client');	
		$oContainer     	    	= $this->container;		
	
		$tCommande					= array();
		$iClientId					= $oRequest->get('client_id',0);
		$zCommande					= $oRequest->get('commande',"");
		
		$oClient = $oRepClient ->find($iClientId);
		if(!is_object($oClient))
		{
			array_push($tErreur,"Le client est introuvable");
		}
			
		if($zCommande == "")
		{
			array_push($tErreur,"Aucun produit dans la commande");
		}
		
		//format: idproduit1_qte1-idproduit12_qte2
		$tListProduitCommande = explode('-', $zCommande);
		foreach($tListProduitCommande as $zProduitCommande)
		{
			$tProduitCommande = explode('_',$zProduitCommande);
			if(isset($tProduitCommande[0]) && isset($tProduitCommande[1]))
			{
				$tCommande[$tProduitCommande[0]] = $tProduitCommande[1];
			}
		}

		
		if(sizeof($tErreur) > 0)
		{
			$zMessage =  'Une erreur s\'est produite lors de l\'enregistrement :' ;
			foreach($tErreur as $zErreur)
			{
				$zMessage.=' - '.$zErreur;
			}
		}
		else
		{						
			$oRepCommande 	= $oEm->getRepository('ChallengeCommandeBundle:Commande');		
			$oCommande     	= $oRepCommande->saveCommande($iClientId, $tCommande);
			
			if($oCommande)
			{
				$zMessage =  'Commande effectuée avec succès !';
				$tResult['id'] = $oCommande->getId();
			}else
			{
				$iCode 	= 403;
				$zMessage =  'Une erreur s\'est produite lors de l\'enregistrement !' ;
			}
			
		}
	
		parent::setCode($iCode);
		parent::setMessage($zMessage);
		parent::setResults($tResult);
		$zResult  = parent::getFormatedResults();
		return new Response( $zResult, 200, array ('Content-Type' => 'application/json'));
	}

}
