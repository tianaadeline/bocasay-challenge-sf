<?php

namespace Challenge\ApiBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
class ApiController implements ContainerAwareInterface
{
    /**
     * @var code
     */
    protected $code = null;
	
	/**
     * @var message
     */
    protected $message = null;
	
	/**
     * @var result
     */
    protected $results = array();
	
	
	/**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
	
	/**
     * Sets the code.
     *
     * @param integer $_iCode
    */
    public function setCode($_iCode = null)
    {
        $this->code = $_iCode;
    }
	
	/**
     * Sets the message.
     *
     * @param integer $_zMessage
    */
    public function setMessage($_zMessage = null)
    {
        $this->message = $_zMessage;
    }
	
	/**
     * sets the results
     *
     * @param array $_tResults
    */
    public function setResults($_tResults= array())
    {
        $this->results = $_tResults;
    }
	
	
	/**
     * format the results
     *
     * @return  json $_zResult
    */
    public function getFormatedResults()
    {
        $tReturn = array();
		$tHead	 = array(
						"code"		=> $this->code,
						"message"	=> $this->message
					);
		
		$tReturn['head'] 	= $tHead;

		if($this->results != null)
		{			
			if($this->results == "true")
			{
				$tReturn['result']	= true;
			}
			elseif($this->results == "false")
			{
				$tReturn['result']	= false;
			}
			else
			{
				$tReturn['result']	= $this->results;
			}
		}
        else{
            $tReturn['result']  = false;
        }
		return json_encode ($tReturn);
    }
}
