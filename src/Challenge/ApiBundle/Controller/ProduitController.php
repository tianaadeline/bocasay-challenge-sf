<?php

namespace Challenge\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Challenge\ApiBundle\Controller\ApiController as ApiController;
class ProduitController extends ApiController
{

	/*
	* Point d'entrée ajout/modification produit
	*/
	public function saveProduitAction(Request $oRequest){

		$iCode 	= 200;
        $zMessage = null;
		$tResult  = array();
		$tErreur = array();
		
		$oEm     			    	= $this->container->get('doctrine')->getEntityManager();
		$tProduit					= array();
		$tProduit['iProduitId']		= $oRequest->get('id',0);
		$tProduit['zTitre']			= $oRequest->get('titre');
		$tProduit['zDescription']	= $oRequest->get('description');
		$tProduit['fPrixTTC']		= $oRequest->get('prix');
		$tProduit['iStock']			= $oRequest->get('stock');
		$tProduit['iTypesId']		= $oRequest->get('types_id');
		$tProduit['iGenreId']		= $oRequest->get('genre_id');
		$tProduit['iMarqueId']		= $oRequest->get('marque_id');
		

		if($tProduit['zTitre']== '')
		{
			array_push($tErreur,"Le titre est obligatoire");
		}
			
		if($tProduit['fPrixTTC']== '' )
		{
			array_push($tErreur,"Le prix est obligatoire");
		}
		
		if($tProduit['iStock']== '' )
		{
			array_push($tErreur,"Le stock est obligatoire");
		}
				
		if(sizeof($tErreur) > 0)
		{
			$iCode 	= 403;
			$zMessage =  'Une erreur s\'est produite lors de l\'enregistrement :' ;
			foreach($tErreur as $zErreur)
			{
				$zMessage.=' - '.$zErreur;
			}
		}
		else
		{
			
			
				$oRepProduit    = $oEm->getRepository('ChallengeProduitBundle:Produit');			
				$oProduit      	= $oRepProduit->saveProduit($tProduit);
			
			if( $oProduit->getId() > 0 )
			{
				$zMessage =  'Enregistrement effectué avec succès !';
				$tResult['id'] = $oProduit->getId();
			}else
			{
				$iCode 	= 403;
				$zMessage =  'Une erreur s\'est produite lors de l\'enregistrement !' ;
			}
			
		}
	
		parent::setCode($iCode);
		parent::setMessage($zMessage);
		parent::setResults($tResult);
		$zResult  = parent::getFormatedResults();
		return new Response( $zResult, 200, array ('Content-Type' => 'application/json'));
	}
	
	
	
	/*
	* Point d'entrée getTypes
	*/
	public function getTypesAction(Request $oRequest){

		$iCode 	= 200;
        $zMessage = null;
		$tResult  = array();

		
		$oEm     		= $this->container->get('doctrine')->getEntityManager();
		$oRepTypes    	= $oEm->getRepository('ChallengeProduitBundle:Types');			
		$toTypes      	= $oRepTypes->getListTypes();
		

		if(sizeof($toTypes) > 0)
		{
			foreach($toTypes as $oType)
			{
				array_push( $tResult,
							array("id"	    => $oType->getId(), 
								  "libelle" => $oType->getLibelle())
						  );
			}
		}

		parent::setCode($iCode);
		parent::setMessage($zMessage);
		parent::setResults($tResult);
		$zResult  = parent::getFormatedResults();
		return new Response( $zResult, 200, array ('Content-Type' => 'application/json'));
	}
	
	
	/*
	* Point d'entrée getGenre
	*/
	public function getGenresAction(Request $oRequest){

		$iCode 	= 200;
        $zMessage = null;
		$tResult  = array();

		
		$oEm     		= $this->container->get('doctrine')->getEntityManager();		
		$oRepGenre    	= $oEm->getRepository('ChallengeProduitBundle:Genre');			
		$toGenre     	= $oRepGenre->getListGenre();
		

		if(sizeof($toGenre) > 0)
		{
			foreach($toGenre as $oGenre)
			{
				array_push( $tResult,
							array("id"	    => $oGenre->getId(), 
								  "libelle" => $oGenre->getLibelle())
						  );
			}
		}

		parent::setCode($iCode);
		parent::setMessage($zMessage);
		parent::setResults($tResult);
		$zResult  = parent::getFormatedResults();
		return new Response( $zResult, 200, array ('Content-Type' => 'application/json'));
	}
	
	
	/*
	* Point d'entrée getMarques
	*/
	public function getMarquesAction(Request $oRequest){

		$iCode 	= 200;
        $zMessage = null;
		$tResult  = array();
		
		$oEm     		= $this->container->get('doctrine')->getEntityManager();
		$oRepMarque    	= $oEm->getRepository('ChallengeProduitBundle:Marque');			
		$toMarque     	= $oRepMarque->getListMarque();
		

		if(sizeof($toMarque) > 0)
		{
			foreach($toMarque as $oMarque)
			{
				array_push( $tResult,
							array("id"	    => $oMarque->getId(), 
								  "libelle" => $oMarque->getLibelle())
						  );
			}
		}

		parent::setCode($iCode);
		parent::setMessage($zMessage);
		parent::setResults($tResult);
		$zResult  = parent::getFormatedResults();
		return new Response( $zResult, 200, array ('Content-Type' => 'application/json'));
	}	
	
	
	/*
	* Point d'entrée liste des getProduits
	*/
	public function getProduitsAction(Request $oRequest){

		$iCode 	= 200;
        $zMessage = null;
		$tResult  = array();
		
		$oEm     		= $this->container->get('doctrine')->getEntityManager();
		$oRepProduit    = $oEm->getRepository('ChallengeProduitBundle:Produit');			
		$toProduit     	= $oRepProduit->getListProduit();
		

		if(sizeof($toProduit) > 0)
		{
			foreach($toProduit as $oProduit)
			{
				array_push( $tResult,
							array("id"	    => $oProduit->getId(), 
								  "titre"   => $oProduit->getTitre())
						  );
			}
		}

		parent::setCode($iCode);
		parent::setMessage($zMessage);
		parent::setResults($tResult);
		$zResult  = parent::getFormatedResults();
		return new Response( $zResult, 200, array ('Content-Type' => 'application/json'));
	}
	
	/*
	* Point d'entrée detail produit getProduit
	*/
	public function getProduitAction(Request $oRequest){

		$iCode 	= 200;
        $zMessage = null;
		$tResult  = array();
		
		
		$oEm     		= $this->container->get('doctrine')->getEntityManager();
		$oRepProduit    = $oEm->getRepository('ChallengeProduitBundle:Produit');
		$iProduitId 	= $oRequest->get('id',0);
		$oProduit     	= $oRepProduit->find($iProduitId);
		

		if(is_object($oProduit))
		{
			
			
			$tProduit = array(	"id"			=> $oProduit->getId(), 
								"titre"   	  	=> $oProduit->getTitre(),
								"description"   => $oProduit->getDescription(),
								"prix"	  		=> $oProduit->getPrixTTC(),
								"stock"			=> $oProduit->getStock(),
								"types_id"		=> (is_object($oProduit->getTypes()))?$oProduit->getTypes()->getId():0,
								"genre_id"		=> (is_object($oProduit->getGenre()))?$oProduit->getGenre()->getId():0,
								"marque_id"		=> (is_object($oProduit->getMarque()))?$oProduit->getMarque()->getId():0,
							);
			
			$tResult = $tProduit;
			
		}

		parent::setCode($iCode);
		parent::setMessage($zMessage);
		parent::setResults($tResult);
		$zResult  = parent::getFormatedResults();
		return new Response( $zResult, 200, array ('Content-Type' => 'application/json'));
	}
}
