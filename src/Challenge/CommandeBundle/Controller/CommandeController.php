<?php

namespace Challenge\CommandeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Challenge\CommandeBundle\Entity\Client;
use Challenge\ProduitBundle\Entity\Produit;
class CommandeController extends Controller
{
    public function inscriptionAction(Request $oRequest)
    {
		$oContainer  = $this->container;
		$oEm         = $this->getDoctrine()->getManager();				
		$aoAssign    = array( 
							
					); 

		return $this->render( '@ChallengeCommande/Front/Page/inscription.html.twig', $aoAssign );
    }
	
	/**
	 * save client
	 * @param  Request $oRequest
	 * @return 
	 */
    public function saveClientAction(Request $oRequest){

		$oEm						= $this->getDoctrine()->getManager();	
		$tClient					= array();
		
		$tClient['zNom']			= $oRequest->get('nom');
		$tClient['zPrenom']			= $oRequest->get('prenom');
		$tClient['zAdresse']		= $oRequest->get('adresse');
		$tClient['zEmail']			= $oRequest->get('email');
		$tClient['iTelephone']		= $oRequest->get('telephone');
		
		
		
				
		$oRepClient    = $oEm->getRepository('ChallengeCommandeBundle:Client');			
		$oClient       = $oRepClient->saveClient($tClient);
		$iRedirectId = 0;
		

		if( $oClient->getId() > 0 ){
			$this->get('session')->getFlashBag()->set('success', 'Inscription effectué avec succès !');
			$iRedirectId = $oClient->getId();
			return $this->redirectToRoute('challenge_commande_listeProduit', array( '_iClientId' => $iRedirectId ));
		}else{
			$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de l\'enregistrement !');
			return $this->redirectToRoute('challenge_commande_inscription');
		}
		
    }
	
	
	public function listeProduitAction($_iClientId, Request $oRequest)
    {
		$oContainer  = $this->container;
		$oEm         = $this->getDoctrine()->getManager();		
		$oRepProduit = $oEm->getRepository('ChallengeProduitBundle:Produit');
		$aoProduit   = $oRepProduit->getListProduit();

		$oRepClient    = $oEm->getRepository('ChallengeCommandeBundle:Client');			
		$oClient       = $oRepClient->find($_iClientId);

		if(is_object($oClient))
		{
			

			$aoAssign    = array( 
						'aoProduit'   => $aoProduit,
						'iClientId'  => $_iClientId
				); 

		
			
			return $this->render( '@ChallengeCommande/Front/Page/listeProduit.html.twig', $aoAssign );			
		}
		
		return $this->redirectToRoute('challenge_commande_inscription');			
		
    }
	

	
	public function ficheProduitAction($_iClientId, $_iProduitId, Request $oRequest)
    {
		$oContainer  = $this->container;
		$oEm         = $this->getDoctrine()->getManager();		
		$oRepProduit = $oEm->getRepository('ChallengeProduitBundle:Produit');
		$oProduit    = $oRepProduit->find($_iProduitId);

		$oRepClient  = $oEm->getRepository('ChallengeCommandeBundle:Client');			
		$oClient     = $oRepClient->find($_iClientId);	

		
		$oSession 	= new Session();
		$iQuantite 	= 1;
		$bMaj		= 0; 
		if($oSession->has('tSessionCommande'))
		{
			$tSessionCommande = $oSession->get('tSessionCommande');
			if (array_key_exists($_iProduitId, $tSessionCommande)) 
			{
				$iQuantite	= $tSessionCommande[$_iProduitId];
				$bMaj		= 1;
			}
        }
				
		$aoAssign    = array( 
					'oProduit'   => $oProduit,
					'iClientId'  => $_iClientId,
					'iQuantite'  => $iQuantite,
					'bMaj'		 => $bMaj
			); 
		
		if(!is_object($oClient))
		{
			return $this->redirectToRoute('challenge_commande_inscription');			
		}
					
		if(!is_object($oProduit))
		{
			return $this->redirectToRoute('challenge_commande_listeProduit', $aoAssign );			
		}
		
		return $this->render( '@ChallengeCommande/Front/Page/ficheProduit.html.twig', $aoAssign );	

    }
	
	
	/**
	 * save produit to commande
	 * @param  Request $oRequest
	 * @return 
	 */
    public function saveToCommandeAction(Request $oRequest){

		$oEm				= $this->getDoctrine()->getManager();	
		$tClient			= array();
		
		$iClientId			= $oRequest->get('iClientId');
		$iProduitId			= $oRequest->get('iProduitId');
		$iQuantite			= $oRequest->get('iQuantite');		
						
				
		
		$oRepClient    		= $oEm->getRepository('ChallengeCommandeBundle:Client');			
		$oClient      		= $oRepClient->find($iClientId);	

		
		$oSession = new Session();
		if($oSession->has('tSessionCommande'))
		{
			$tSessionCommande = $oSession->get('tSessionCommande');	
		}
		else
		{
			$tSessionCommande = array();	
		}
		
		
		$tSessionCommande[$iProduitId] = $iQuantite;
		$oSession->set('tSessionCommande', $tSessionCommande);
			

		if( $oClient->getId() > 0 ){			
			return $this->redirectToRoute('challenge_commande_panier', array( '_iClientId' => $iClientId ));
		}else{
			$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de l\'enregistrement !');
			return $this->redirectToRoute('challenge_commande_inscription');
		}
		
    }

	public function panierAction($_iClientId, Request $oRequest)
    {
		$oContainer  = $this->container;
		$oEm         = $this->getDoctrine()->getManager();		
		$oRepProduit = $oEm->getRepository('ChallengeProduitBundle:Produit');
		
		$oRepClient  = $oEm->getRepository('ChallengeCommandeBundle:Client');			
		$oClient     = $oRepClient->find($_iClientId);

		$aoProduit   = array();
		if(is_object($oClient))
		{
			$oSession = new Session();
			if($oSession->has('tSessionCommande'))
			{
				$tSessionCommande = $oSession->get('tSessionCommande');
				foreach($tSessionCommande as $iProduitId => $iQuantite)
				{
					
					$oProduit = $oRepProduit->find($iProduitId);
					if(is_object($oProduit))
					{
						
						$tProduit = array(
										'iQuantite' => $iQuantite,
										'oProduit'	=> $oProduit
									);
						array_push($aoProduit, $tProduit );
					}
					
				}
			}
			
			$aoAssign    = array( 
						'aoProduit'   => $aoProduit,
						'iClientId'  => $_iClientId
				); 					
			return $this->render( '@ChallengeCommande/Front/Page/panier.html.twig', $aoAssign );			
		}
		
		return $this->redirectToRoute('challenge_commande_inscription');					
    }


	/**
	 * save commande
	 * @param  Request $oRequest
	 * @return 
	 */
    public function saveCommandeAction($_iClientId,Request $oRequest){

		$oEm				= $this->getDoctrine()->getManager();	
				
		$oRepCommande  		= $oEm->getRepository('ChallengeCommandeBundle:Commande');								
		$oSession = new Session();
		if($oSession->has('tSessionCommande'))
		{
			$tSessionCommande = $oSession->get('tSessionCommande');
			$oCommande  		= $oRepCommande->saveCommande($_iClientId, $tSessionCommande );
			if(!is_object($oCommande))
			{
				$this->get('session')->getFlashBag()->set('danger', 'Votre commande n\'a pas pu être enregistrée !');
			}
			else
			{
				$this->get('session')->getFlashBag()->set('success', 'Votre commande est enregistrée" !');	
			}	
			$oSession->remove('tSessionCommande');
		}else{
			$this->get('session')->getFlashBag()->set('danger', 'Une erreur s\'est produite lors de l\'enregistrement!');
			
		}
		
		return $this->redirectToRoute('challenge_commande_listeProduit', array( '_iClientId' => $_iClientId ));
    }
	
	/**
	 * supprimer produit to commande
	 * @param  Request $oRequest
	 * @return 
	 */
    public function supprimerProduitPanierAction($_iClientId, $_iProduitId, Request $oRequest){

		$oEm				= $this->getDoctrine()->getManager();	
		
		$oSession = new Session();
		if($oSession->has('tSessionCommande'))
		{
			$tSessionCommande = $oSession->get('tSessionCommande');	
			if (array_key_exists($_iProduitId, $tSessionCommande)) 
			{
				unset($tSessionCommande[$_iProduitId]);			
				$oSession->set('tSessionCommande',$tSessionCommande);
			}
		}		
		return $this->redirectToRoute('challenge_commande_panier', array( '_iClientId' => $_iClientId ));		
    }

}
