<?php

namespace Challenge\CommandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProduitCommande
 *
 * @ORM\Table(name="produitCommande")
 * @ORM\Entity(repositoryClass="Challenge\CommandeBundle\Repository\ProduitCommandeRepository")
 */
class ProduitCommande
{
    /**
     * @var int
     *
     * @ORM\Column(name="produitCommande_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="produitCommande_prixUnitaire", type="float")
     */
    private $prixUnitaire;

    /**
     * @var int
     *
     * @ORM\Column(name="produitCommande_quantite", type="integer")
     */
    private $quantite;

	/**
     * @var \Produit
     *
     * @ORM\ManyToOne(targetEntity="Challenge\ProduitBundle\Entity\Produit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="produitCommande_produitId", referencedColumnName="produit_id")
     * })
     */
    private $produit;
	
	/**
     * @var \Commande
     *
     * @ORM\ManyToOne(targetEntity="Challenge\CommandeBundle\Entity\Commande")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="produitCommande_commandeId", referencedColumnName="commande_id")
     * })
     */
    private $commande;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prixUnitaire
     *
     * @param float $prixUnitaire
     *
     * @return ProduitCommande
     */
    public function setPrixUnitaire($prixUnitaire)
    {
        $this->prixUnitaire = $prixUnitaire;
    
        return $this;
    }

    /**
     * Get prixUnitaire
     *
     * @return float
     */
    public function getPrixUnitaire()
    {
        return $this->prixUnitaire;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return ProduitCommande
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;
    
        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set produit
     *
     * @param \Challenge\ProduitBundle\Entity\Produit $produit
     *
     * @return ProduitCommande
     */
    public function setProduit(\Challenge\ProduitBundle\Entity\Produit $produit = null)
    {
        $this->produit = $produit;
    
        return $this;
    }

    /**
     * Get produit
     *
     * @return \Challenge\ProduitBundle\Entity\Produit
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Set commande
     *
     * @param \Challenge\CommandeBundle\Entity\Commande $commande
     *
     * @return ProduitCommande
     */
    public function setCommande(\Challenge\CommandeBundle\Entity\Commande $commande = null)
    {
        $this->commande = $commande;
    
        return $this;
    }

    /**
     * Get commande
     *
     * @return \Challenge\CommandeBundle\Entity\Commande
     */
    public function getCommande()
    {
        return $this->commande;
    }
}
