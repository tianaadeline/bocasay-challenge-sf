<?php

namespace Challenge\CommandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commande
 *
 * @ORM\Table(name="commande")
 * @ORM\Entity(repositoryClass="Challenge\CommandeBundle\Repository\CommandeRepository")
 */
class Commande
{
    /**
     * @var int
     *
     * @ORM\Column(name="commande_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="commande_date", type="datetime")
     */
    private $date;

    /**
     * @var float
     *
     * @ORM\Column(name="commande_montantTotal", type="float" , nullable=true)
     */
    private $montantTotal;
	
    /**
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="Challenge\CommandeBundle\Entity\Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="commande_clientId", referencedColumnName="client_id")
     * })
     */
    private $client;
	
	
    /**
     * @var Challenge\CommandeBundle\Entity\ProduitCommande
     * @ORM\OneToMany(targetEntity="Challenge\CommandeBundle\Entity\ProduitCommande", mappedBy="commande")
     * @ORM\JoinTable(name="produitCommande")
     */
    protected $produitCommande;
	
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Commande
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

	
	/**
     * Set montantTotal
     *
     * @param float $montantTotal
     *
     * @return ProduitCommande
     */
    public function setMontantTotal($montantTotal)
    {
        $this->montantTotal = $montantTotal;
    
        return $this;
    }

    /**
     * Get montantTotal
     *
     * @return float
     */
    public function getMontantTotal()
    {
        return $this->montantTotal;
    }
	
	
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->produitCommande = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set client
     *
     * @param \Challenge\CommandeBundle\Entity\Client $client
     *
     * @return Commande
     */
    public function setClient(\Challenge\CommandeBundle\Entity\Client $client = null)
    {
        $this->client = $client;
    
        return $this;
    }

    /**
     * Get client
     *
     * @return \Challenge\CommandeBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Add produitCommande
     *
     * @param \Challenge\CommandeBundle\Entity\ProduitCommande $produitCommande
     *
     * @return Commande
     */
    public function addProduitCommande(\Challenge\CommandeBundle\Entity\ProduitCommande $produitCommande)
    {
        $this->produitCommande[] = $produitCommande;
    
        return $this;
    }

    /**
     * Remove produitCommande
     *
     * @param \Challenge\CommandeBundle\Entity\ProduitCommande $produitCommande
     */
    public function removeProduitCommande(\Challenge\CommandeBundle\Entity\ProduitCommande $produitCommande)
    {
        $this->produitCommande->removeElement($produitCommande);
    }

    /**
     * Get produitCommande
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduitCommande()
    {
        return $this->produitCommande;
    }
}
